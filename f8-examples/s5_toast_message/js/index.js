import {toast} from "./toast.js";

function showSuccessToast() {
    toast({
        title: "Thành công!",
        message: "Bạn đã đăng ký thành công.",
        type: "success",
        duration: 5000
    });
}

function showErrorToast() {
    toast({
        title: "Thất bại!",
        message: "Có lỗi xảy ra, vui lòng liên hệ quản trị viên.",
        type: "error",
        duration: 5000
    });
}

document.querySelector('.btn--success').addEventListener('click', showSuccessToast);
document.querySelector('.btn--danger').addEventListener('click', showErrorToast);