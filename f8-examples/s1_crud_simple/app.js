const apiURL = "http://localhost:3000/courses";
var ulElement = document.querySelector('#course-list');
var btnSubmitElement = document.querySelector('#btn-submit');
var title = document.querySelector('input[name="title"]');
var description = document.querySelector('input[name="description"]');

function start() {
    getCourses(renderCourses);
}

start();

btnSubmitElement.addEventListener('click', function (e) {
    e.preventDefault();
    var data = {
        title: title.value,
        description: description.value,
    };

    if (e.target.classList.contains('btn-update-element')) {
        var updateId = document.querySelector('.hidden-update-id').value;
        updateCourse(updateId, data, handleReRender);

        btnSubmitElement.textContent = 'Create';
        btnSubmitElement.classList.remove('btn-update-element');
    } else {
        createCourse(data, handleReRender);
    }

    title.value = '';
    description.value = '';
});

// Functions
function getCourses(callback) {
    fetch(apiURL).then(function (response) {
        return response.json();
    }).then(callback);
}

function renderCourses(courses) {
    var html = courses.map(function (course) {
        return `
            <li>
                <h4>${course.title}</h4>
                <p>${course.description}</p>
                <button class="btn-delete" data-id="${course.id}">Delete</button>
                <button class="btn-update" data-id="${course.id}">Update</button>
            </li>
        `;
    });

    ulElement.innerHTML = html.join('');

    localStorage.setItem("courses", JSON.stringify(courses));

    var btnDeleteList = document.querySelectorAll('.btn-delete');
    btnDeleteList.forEach(function (btn) {
        btn.addEventListener('click', deleteCourse);
    });

    var btnUpdateList = document.querySelectorAll('.btn-update');
    btnUpdateList.forEach(function (btn) {
        btn.addEventListener('click', handleUpdate);
    });
}

function handleUpdate(event) {
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var hiddenField = document.querySelector('#hidden-field');
    var inputElement = hiddenField.appendChild(document.createElement('input'));
    inputElement.id = `update-${id}`;
    inputElement.classList.add('hidden-update-id');
    inputElement.hidden = true;
    inputElement.value = id;

    var course = getCoursesFromLocalStorage().filter(function (course) {
        return course.id === parseInt(id);
    })[0];

    title.value = course.title;
    description.value = course.description;
    btnSubmitElement.textContent = 'Update';
    btnSubmitElement.classList.add('btn-update-element');

    var btnUpdateList = document.querySelectorAll('.btn-update');
    btnUpdateList.forEach(function (btn) {
        btn.remove();
    });
}

function deleteCourse(event) {
    event.preventDefault();
    var id = event.target.getAttribute('data-id');

    fetch(apiURL + `/${id}`, {method: 'DELETE'})
        .then(function (response) {
            return response.json()
        }).then(function () {
        handleReRender(id);
    });
}

function updateCourse(id, data, callback) {
    var options = {
        method: "PATCH", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    };

    fetch(apiURL + `/${id}`, options)
        .then(function (response) {
            return response.json();
        }).then(callback);
}

function createCourse(data, callback) {
    var options = {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    };

    fetch(apiURL, options)
        .then(function (response) {
            return response.json();
        }).then(callback);
}

function handleReRender(data) {
    var courses = getCoursesFromLocalStorage();

    if (typeof data === 'object') {
        var isUpdate = courses.some(function (course) {
            return course.id === data.id;
        });

        if (isUpdate) {
            courses = courses.map(function (course) {
                if (course.id === data.id) {
                    return data;
                }
                return course;
            });
        } else {
            courses.push(data);
        }
    } else {
        courses = courses.filter(function (course) {
            return course.id !== parseInt(data);
        });
    }

    localStorage.setItem("courses", JSON.stringify(courses));
    renderCourses(courses);
}

function getCoursesFromLocalStorage() {
    const coursesJSON = localStorage.getItem("courses");
    if (coursesJSON) {
        return JSON.parse(coursesJSON);
    } else {
        return [];
    }
}
