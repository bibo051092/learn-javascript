const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const PLAYLIST_CONFIG = 'playlist_config';

const playerElement = $('.player');
const playlistElement = $('.playlist');
const cdElement = $('.cd');
const headingElement = $('header h2');
const cdThumbElement = $('.cd-thumb');
const audioElement = $('#audio');
const progressElement = $('#progress');
const playBtn = $('.btn-toggle-play');
const nextBtn = $('.btn-next');
const prevBtn = $('.btn-prev');
const randomBtn = $('.btn-random');
const repeatBtn = $('.btn-repeat');

const app = {
    currentIndex: 0,
    isPlaying: false,
    isRandom: false,
    isRepeat: false,
    config: JSON.parse(localStorage.getItem(PLAYLIST_CONFIG)) || {},
    setConfig: function (key, value) {
        this.config[key] = value;
        localStorage.setItem(PLAYLIST_CONFIG, JSON.stringify(this.config));
    },
    songs: [
        {
            name: "Sau Này Chúng Ta Giàu",
            singer: "Khắc Việt, ACV",
            path: "./assets/audio/sau-nay-chung-ta-giau.mp3",
            image: "./assets/images/sau-nay-chung-ta-giau.jpg",
            isListened: false,
        },
        {
            name: "Ngày mai người ta lấy chồng",
            singer: "Thành Đạt",
            path: "./assets/audio/ngay-mai-nguoi-ta-lay-chong.mp3",
            image: "./assets/images/ngay-mai-nguoi-ta-lay-chong.jpg",
            isListened: false,
        },
        {
            name: "Ngày mai người ta lấy chồng 1",
            singer: "Thành Đạt",
            path: "./assets/audio/ngay-mai-nguoi-ta-lay-chong.mp3",
            image: "./assets/images/ngay-mai-nguoi-ta-lay-chong.jpg",
            isListened: false,
        },
        {
            name: "Ngày mai người ta lấy chồng 2",
            singer: "Thành Đạt",
            path: "./assets/audio/ngay-mai-nguoi-ta-lay-chong.mp3",
            image: "./assets/images/ngay-mai-nguoi-ta-lay-chong.jpg",
            isListened: false,
        },
        {
            name: "Ngày mai người ta lấy chồng 3",
            singer: "Thành Đạt",
            path: "./assets/audio/ngay-mai-nguoi-ta-lay-chong.mp3",
            image: "./assets/images/ngay-mai-nguoi-ta-lay-chong.jpg",
            isListened: false,
        },
    ],
    start: function () {
        this.loadConfig();
        this.render()

        this.defineProperties();
        this.handleEvents();

        this.loadCurrentSong();

        randomBtn.classList.toggle('active', app.isRandom);
        repeatBtn.classList.toggle('active', app.isRepeat);
    },
    render: function () {
        const html = this.songs.map(function (song, index) {
            return `
            <div class="song ${index === app.currentIndex ? 'active' : ''}" data-index="${index}">
                <div class="thumb" style="background-image: url(${song.image})"></div>
                <div class="body">
                    <h3 class="title">${song.name}</h3>
                    <p class="author">${song.singer}</p>
                </div>
                <div class="option">
                    <i class="fas fa-ellipsis-h"></i>
                </div>
            </div>
            `;
        });

        playlistElement.innerHTML = html.join('');
    },
    handleEvents: function () {
        const cdWidth = cdElement.offsetWidth;

        const cdThumbAnimate = cdThumbElement.animate([
            {
                transform: 'rotate(360deg)'
            }
        ], {
            duration: 10000, // 10 seconds
            iterations: Infinity,
        });
        cdThumbAnimate.pause();

        document.onscroll = function () {
            const scrollTop = window.scrollY || document.documentElement.scrollTop;
            const newCdWidth = cdWidth - scrollTop;

            cdElement.style.width = (newCdWidth > 0) ? newCdWidth + 'px' : 0;
            cdElement.style.opacity = newCdWidth / cdWidth;
        };

        playBtn.addEventListener('click', function () {
            if (app.isPlaying) {
                audioElement.pause();
            } else {
                audioElement.play();
            }
        });

        audioElement.addEventListener('play', function () {
            app.isPlaying = true;
            playerElement.classList.add('playing');
            cdThumbAnimate.play();
        });

        audioElement.addEventListener('pause', function () {
            app.isPlaying = false;
            playerElement.classList.remove('playing');
            cdThumbAnimate.pause();
        });

        audioElement.addEventListener('timeupdate', function () {
            if (audioElement.duration) {
                progressElement.value = Math.floor(audioElement.currentTime / audioElement.duration * 100);
            }
        });

        progressElement.addEventListener('change', function (e) {
            audioElement.currentTime = audioElement.duration / 100 * e.target.value;
        });

        nextBtn.addEventListener('click', function () {
            if (app.isRandom) {
                app.handleRandomSong();
            } else {
                app.nextSong();
            }

            audioElement.play();
            app.render();
            app.scrollToActiveSong();
        });

        prevBtn.addEventListener('click', function () {
            if (app.isRandom) {
                app.handleRandomSong();
            } else {
                app.prevSong();
            }

            audioElement.play();
            app.render();
            app.scrollToActiveSong();
        });

        randomBtn.addEventListener('click', function () {
            app.isRandom = !app.isRandom;
            app.setConfig('isRandom', app.isRandom);
            this.classList.toggle('active', app.isRandom);
        });

        repeatBtn.addEventListener('click', function () {
            app.isRepeat = !app.isRepeat;
            app.setConfig('isRepeat', app.isRepeat);
            this.classList.toggle('active', app.isRepeat);
        });

        audioElement.addEventListener('ended', function () {
            if (app.isRepeat) {
                audioElement.play();
            } else {
                nextBtn.click();
            }
        });

        playlistElement.addEventListener('click', function (e) {
            const songElement = e.target.closest('.song:not(.active)');
            const optionsElement = e.target.closest('.option');
            if (songElement || optionsElement) {
                if (songElement) {
                    app.currentIndex = parseInt(songElement.dataset.index);
                    app.loadCurrentSong();
                    app.render();
                    audioElement.play();
                }

                if (optionsElement) {
                    // todo options playlist
                }
            }
        });
    },
    defineProperties: function () {
        Object.defineProperty(this, 'currentSong', {
            get: function () {
                if (this.isRandom) {
                    const listenedSongs = this.songs.filter(song => !song.isListened);
                    return listenedSongs[this.currentIndex];
                }

                return this.songs[this.currentIndex];
            }
        });
    },
    loadCurrentSong: function () {
        headingElement.innerText = this.currentSong.name;
        cdThumbElement.style.backgroundImage = `url('${this.currentSong.image}')`;
        audioElement.src = this.currentSong.path;
    },
    nextSong: function () {
        this.currentIndex++;

        if (this.currentIndex >= this.songs.length) {
            this.currentIndex = 0;
        }
        this.loadCurrentSong();
    },
    prevSong: function () {
        this.currentIndex--;

        if (this.currentIndex < 0) {
            this.currentIndex = this.songs.length - 1;
        }
        this.loadCurrentSong();
    },
    handleRandomSong: function () {
        let newIndexSong = 0;
        let songs = this.songs;

        if (this.isRandom) {
            let isListedFullSongs = this.songs.every(song => song.isListened);

            if (isListedFullSongs) {
                this.songs.map(function (song) {
                    song.isListened = false;
                    return song;
                });
            }

            songs = songs.filter(song => !song.isListened);
        }

        do {
            newIndexSong = Math.floor(Math.random() * songs.length);
        } while (newIndexSong === this.currentIndex);

        this.currentIndex = newIndexSong;
        this.loadCurrentSong();

        if (this.isRandom) {
            this.songs.map(function (song) {
                if (song.name === songs[newIndexSong].name) {
                    song.isListened = true;
                }
                return song;
            });
        }
    },
    scrollToActiveSong: function () {
        setTimeout(() => {
            $('.song.active').scrollIntoView({
                behavior: "smooth",
                block: "center",
            });
        }, 300);
    },
    loadConfig: function () {
        this.isRandom = (this.config.isRandom) ?? false;
        this.isRepeat = (this.config.isRepeat) ?? false;
    }
};

app.start();


