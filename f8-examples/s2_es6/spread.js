//// Array
let array1 = ['Javascript', 'Ruby', 'PHP'];
let array2 = ['ReactJS', 'Dart'];
let array3 = [...array1, ...array2]; // ['Javascript', 'Ruby', 'PHP', 'ReactJS', 'Dart']

//// Object
let object1 = {name: 'Javascript',};
let object2 = {price: 1000,};
let object3 = {...object1, ...object2}; // {name: 'Javascript', price: 1000,}

//// examples
const defaultConfig = {
    api: 'http://localhost:3000',
    apiVersion: 'v1',
    other: 'other',
};

const exerciseConfig = {
  ...defaultConfig,
  api: 'http://localhost:5000',
};

// other
let array = ['Javascript', 'Ruby', 'PHP'];

function logger(...rest) {} // rest parameter
logger(...array); // spread
