//// return normal
// const sum = (a, b) => {
//     return a + b;
// }

// or

// const sum = (a, b) => a + b;

//// return object
// const course = (a, b) => ({a: a, b: b});

// or

// const course = (a, b) => {
//     return {
//         a: a,
//         b: b,
//     };
// };