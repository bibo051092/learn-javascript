let brand = 'F8';
let course = 'Javascript!';

function highlight([first, ...strings], ...values) {
    return values.reduce((acc, current) => [...acc, `<span>${current}</span>`, strings.shift()], [first]).join('');
}

const html = highlight`Học lập trình ${course} tại ${brand}!`; // Học lập trình <span>Javascipt!</span> tại <span>F8</span>!