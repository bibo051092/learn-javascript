//// Array
let array = ['Javascript', 'PHP', 'Ruby'];
let [a, ...rest] = array; // a = Javascript, rest = ['PHP', 'Ruby']


//// Object
let course = {
    name: 'Javascript',
    price: 1000,
    description: 'Learn JS'
};
let {name, ...rest} = course; /// a = Javascript, rest =  {price: 1000, description: 'Learn JS'}

//// Parameters
function logger(...params) {
    console.log(params); // [1, 2, 3, 4, 5]
}

logger(1, 2, 3, 4, 5);


function loggerObj({name, price, ...rest}) {
    console.log(name); // Javascript
    console.log(price); // 1000
    console.log(rest); // {description: 'Learn JS'}
}

loggerObj({
    name: 'Javascript',
    price: 1000,
    description: 'Learn JS'
});


function loggerArr([a, b, ...rest]) {
    console.log(a); // 1
    console.log(b); // 2
    console.log(rest); // [3, 4, 5]
}

loggerArr([1, 2 ,3 ,4, 5]);
